#include "Bag.h"
#include "Types.h"

/**
 * @brief Constructor of the Bag class.
 * @param amountOfRed Amount of red cubes in the bag.
 * @param amountOfBlue Amount of blue cubes in the bag.
 * @param amountOfGreen Amount of green cubes in the bag.
 */
Bag::Bag(int amountOfRed, int amountOfGreen, int amountOfBlue) {
    this->amountOfCubes = new int[static_cast<int>(Types::Color::amountOfColors)];
    this->amountOfCubes[static_cast<int>(Types::Color::red)] = amountOfRed;
    this->amountOfCubes[static_cast<int>(Types::Color::blue)] = amountOfBlue;
    this->amountOfCubes[static_cast<int>(Types::Color::green)] = amountOfGreen;
}

/**
 * @brief Destructor of the Bag class.
 */
Bag::~Bag() {
    delete[] this->amountOfCubes;
}

/**
 * @brief Returns the amount of cubes of the given color.
 * @param color Color of the cubes.
 * @return Amount of cubes of the given color.
 */
int
Bag::amountOfCubesOfColor(Types::Color color) {
    return this->amountOfCubes[static_cast<int>(color)];
}

/**
 * @brief Checks if the game is possible with the given amount of cubes.
 * @param amountOfRed Amount of red cubes needed to play the game.
 * @param amountOfBlue Amount of blue cubes needed to play the game.
 * @param amountOfGreen Amount of green cubes needed to play the game.
 * @return True if the game is possible, false otherwise.
 */
bool
Bag::isGamePossible(unsigned int amountOfRed, unsigned int amountOfGreen, unsigned int amountOfBlue) {
    return this->amountOfCubesOfColor(Types::Color::red) >= amountOfRed &&
            this->amountOfCubesOfColor(Types::Color::blue) >= amountOfBlue &&
            this->amountOfCubesOfColor(Types::Color::green) >= amountOfGreen;
}