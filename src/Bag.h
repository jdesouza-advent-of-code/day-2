#ifndef BAG_H
#define BAG_H

#include "Types.h"

class Bag {
    private:
        int *amountOfCubes;
    public:
        Bag(int amountOfRed, int amountOfGreen, int amountOfBlue);
        ~Bag();
        int amountOfCubesOfColor(Types::Color color);
        bool isGamePossible(unsigned int amountOfRed, unsigned int amountOfGreen, unsigned int amountOfBlue);
};

#endif // BAG_H