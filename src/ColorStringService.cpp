#include <string>
#include "ColorStringService.h"
#include "Types.h"

/**
 * @brief Get the Color Type object
 * @details Gets the Types::Color value of a string in the format "<amount> <color>".
 * @param colorString String in the format "<amount> <color>".
 * @return Types::Color value of the string. If the string is not in the correct format, it returns Types::Color::amountOfColors.
*/
Types::Color
ColorStringService::getColorType(std::string colorString) {
    std::string colorName = colorString.substr(colorString.find(" ") + 1);

    switch (tolower(colorName[0])) {
        case 'b':
            return Types::Color::blue;
        case 'r':
            return Types::Color::red;
        case 'g':
            return Types::Color::green;
        default:
            return Types::Color::amountOfColors;
    }
}

/**
 * @brief Get the Color Amount object
 * @details Gets the amount of cubes of a string in the format "<amount> <color>".
 * @param colorString String in the format "<amount> <color>".
 * @return Amount of cubes of the string. If the string is not in the correct format, it returns 0.
*/
unsigned int
ColorStringService::getColorAmount(std::string colorString) {
    std::string amountString = colorString.substr(0, colorString.find(" "));

    if (amountString.length() == 0) {
        return 0;
    }

    return std::stoi(amountString);
}
