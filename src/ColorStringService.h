#ifndef COLOR_STRING_SERVICE_H
#define COLOR_STRING_SERVICE_H

#include <string>
#include "Types.h"

class ColorStringService {
    public:
        static Types::Color getColorType(std::string colorString);
        static unsigned int getColorAmount(std::string colorString);
};

#endif // COLOR_STRING_SERVICE_H