#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <string>

namespace Constants {
    const std::string SET_SEPARATOR = ";";
    const std::string COLOR_SEPARATOR = ",";
}

#endif