#include <iostream>
#include "Game.h"
#include "Set.h"
#include "Types.h"

/**
 * @brief Constructor of the Game class.
 * @param id Id of the game.
 */
Game::Game(Types::GameId id) {
    this->id = id;
    this->amountOfSets = 0;
    this->sets = new Set*[0];
}

/**
 * @brief Constructor of the Game class.
 * @param id Id of the game.
 * @param amountOfRed Amount of red cubes in the set.
 * @param amountOfBlue Amount of blue cubes in the set.
 * @param amountOfGreen Amount of green cubes in the set.
 */
Game::Game(Types::GameId id, int amountOfRed, int amountOfGreen, int amountOfBlue) {
    this->id = id;
    this->sets = new Set*[1];
    this->sets[0] = new Set(amountOfRed, amountOfGreen, amountOfBlue);
}

/**
 * @brief Destructor of the Game class.
 */
Game::~Game() {
    for (int i = 0; i < this->amountOfSets; i++) {
        delete this->sets[i];
    }
    delete[] this->sets;
}

/**
 * @brief Returns the id of the game.
 * @return Id of the game.
 */
Types::GameId Game::getId() {
    return this->id;
}

/**
 * @brief Returns the amount of sets in the game.
 * @return Amount of sets in the game.
 */
int Game::getAmountOfSets() {
    return this->amountOfSets;
}

/**
 * @brief Adds a set to the game.
 * @param amountOfRed Amount of red cubes in the set.
 * @param amountOfGreen Amount of green cubes in the set.
 * @param amountOfBlue Amount of blue cubes in the set.
 * @return Error code.
 */
Types::Errors Game::addSet(int amountOfRed, int amountOfGreen, int amountOfBlue) {
    Set **newSets = new Set*[this->amountOfSets + 1];
    for (int i = 0; i < this->amountOfSets; i++) {
        newSets[i] = this->sets[i];
    }
    newSets[this->amountOfSets] = new Set(amountOfRed, amountOfGreen, amountOfBlue);
    delete[] this->sets;
    this->sets = newSets;

    this->amountOfSets += 1;
    return Types::Errors::ok;
}

/**
 * @brief Adds a set to the game.
 * @param set Set to add to the game.
 * @return Error code.
 */
Types::Errors Game::addSet(Set *set) {
    Set **newSets = new Set*[this->amountOfSets + 1];
    for (int i = 0; i < this->amountOfSets; i++) {
        newSets[i] = this->sets[i];
    }
    newSets[this->amountOfSets] = set;
    delete[] this->sets;
    this->sets = newSets;

    this->amountOfSets += 1;
    return Types::Errors::ok;
}

/**
 * @brief Pops a set from the game.
 * @return Set.
 */
Set *Game::popSet() {
    if (this->amountOfSets == 0) {
        return NULL;
    }

    Set *set = this->sets[this->amountOfSets - 1];
    Set **newSets = new Set*[this->amountOfSets - 1];
    for (int i = 0; i < this->amountOfSets - 1; i++) {
        newSets[i] = this->sets[i];
    }
    delete[] this->sets;
    this->sets = newSets;
    this->amountOfSets--;
    return set;
}

/**
 * @brief Returns the maximum amount of cubes of a color in a set.
 * @param color Color of the cubes.
 * @return Maximum amount of cubes of a color in a set.
 */
int
Game::getMaximumAmountOfCubesOfColorInASet(Types::Color color)
{
    unsigned int maximumAmountOfCubesOfColorInASet = 0;
    for (int i = 0; i < this->amountOfSets; i++) {
        unsigned int amountOfCubesOfColorInSet = this->sets[i]->amountOfCubesOfColor(color);
        if (amountOfCubesOfColorInSet > maximumAmountOfCubesOfColorInASet) {
            maximumAmountOfCubesOfColorInASet = amountOfCubesOfColorInSet;
        }
    }
    return maximumAmountOfCubesOfColorInASet;
}