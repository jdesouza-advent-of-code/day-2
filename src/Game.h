#ifndef GAME_H
#define GAME_H

#include "Types.h"
#include "Set.h"

class Game {
    private:
        Types::GameId id;
        Set **sets;
        unsigned int amountOfSets;
    public:
        Game(Types::GameId id);
        Game(Types::GameId id, int amountOfRed, int amountOfGreen, int amountOfBlue);
        ~Game();
        Types::GameId getId();
        int getAmountOfSets();
        Types::Errors addSet(Set *set);
        Types::Errors addSet(int amountOfRed, int amountOfGreen, int amountOfBlue);
        Set *popSet();
        int getMaximumAmountOfCubesOfColorInASet(Types::Color color);
};

#endif // GAME_H
