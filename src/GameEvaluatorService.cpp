#include "GameEvaluatorService.h"
#include "Game.h"
#include "Bag.h"
#include "Set.h"

/**
 * @brief Checks if a game is valid.
 * @param game Game to check.
 * @param bag Bag to check.
 * @return True if the game is valid, false otherwise.
 */
bool
GameEvaluatorService::isGameValid(Game *game, Bag *bag) {
    int amountOfSets = game->getAmountOfSets();
    for (int i = 0; i < amountOfSets; i++) {
        Set *set = game->popSet();
        if (
            !bag->isGamePossible(
                set->amountOfCubesOfColor(Types::Color::red),
                set->amountOfCubesOfColor(Types::Color::green),
                set->amountOfCubesOfColor(Types::Color::blue)
            )
        ) {
            return false;
        }
    }

    return true;
}