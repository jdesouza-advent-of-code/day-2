#ifndef GAME_EVALUATOR_SERVICE_H
#define GAME_EVALUATOR_SERVICE_H

#include "Game.h"
#include "Bag.h"

class GameEvaluatorService {
    public:
        static bool isGameValid(Game *game, Bag *bag);
};

#endif // GAME_EVALUATOR_SERVICE_H