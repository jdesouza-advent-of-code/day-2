#include <string>
#include <fstream>
#include "InputReader.h"
#include "Types.h"
#include "Game.h"
#include "LineProcessingService.h"

/**
 * @brief Constructor of the InputReader class.
 * @param filename Name of the file to read.
 * @param path Path to the file to read.
 */
InputReader::InputReader(char *filename, char *path) {
    this->filename = filename;
    this->path = path;
    this->games = new Game*[0];
}

/**
 * @brief Destructor of the InputReader class.
 */
/**
 * @brief Reads the file and creates the games.
 * @return Error code.
 */
Types::Errors
InputReader::read() {
    std::ifstream file;
    std::string filePath = std::string(this->path) + std::string(this->filename);
    file.open(filePath.c_str());
    
    if (!file.is_open()) {
        return Types::Errors::fileNotFound;
    }
    
    std::string line;
    while (std::getline(file, line)) {
        // Process each line of the file here
        Game *game = LineProcessingService::processLine(line);

        // We need to add the Game to the array of Games.
        Game **newGames = new Game*[this->amountOfGames + 1];
        for (int i = 0; i < this->amountOfGames; i++) {
            newGames[i] = this->games[i];
        }
        newGames[this->amountOfGames] = game;
        this->games = newGames;
        this->amountOfGames++;
    }
    
    file.close();
    
    return Types::Errors::ok;
}

/**
 * @brief Pops a game from the games array.
 * @return Pointer to the game.
 */
Game *
InputReader::popGame() {
    if (this->amountOfGames == 0) {
        return NULL;
    }

    Game *game = this->games[this->amountOfGames - 1];
    this->amountOfGames--;
    return game;
}

/**
 * @brief Gets the amount of games.
 * @return Amount of games.
 */
int
InputReader::getAmountOfGames() {
    return this->amountOfGames;
}