#ifndef INPUT_READER_H
#define INPUT_READER_H

#include "Types.h"
#include "Game.h"

class InputReader {
    private:
        char *filename;
        char *path;
        Game **games;
        int amountOfGames;
    public:
        InputReader(char *filename, char *path);
        ~InputReader();
        Types::Errors read();
        Game *popGame();
        int getAmountOfGames();
};

#endif // INPUT_READER_H