#include <iostream>
#include <string>
#include <exception>
#include "LineProcessingService.h"
#include "ColorStringService.h"
#include "Constants.h"
#include "Types.h"
#include "Game.h"
#include "Set.h"

/**
 * @brief This method processes a line of the input file and generates a Game.
 * @details A line is a string with the following format:
 *          Game <ID>: <amountOfBlue> blue, <amountOfRed> red, <amountOfGreen> green; ...
 *          Where <ID> is the id of the Game, <amountOf"Color"> is the amount of cubes of that color in a Set.
 *          A Set is a group of the three different colors and are divided by a semicolon in the input.
 *          A Game is a group of Sets and are divided by a new line in the input.
 *          A Game can have any amount of Sets.
 *          A Set must have the amount of cubes of each color defined but the order of the colors can change.
 * @param line Line to process.
 * @return Pointer to the game.
 */
Game *
LineProcessingService::processLine(std::string line) {
    // First, we need to get the ID of the Game.
    // The ID will be always after the word "Game " and before the first colon.
    std::string idString = line.substr(line.find("Game") + 5, line.find(":") - 5);
    // We need to convert the string to an integer.
    Types::GameId id = std::stoi(idString);

    // Lets throw away the part of the string we already used.
    line = line.substr(line.find(":") + 2, line.length() - line.find(":") - 2);

    // Now, we need to break the line into Sets.
    // We can do this by splitting the line by the semicolon.
    std::string *sets = new std::string[0];
    int amountOfSets = 0;
    while (line.find(Constants::SET_SEPARATOR) != std::string::npos) {
        // We need to get the first Set.
        std::string set = line.substr(0, line.find(Constants::SET_SEPARATOR));

        // We need to add the Set to the array of Sets.
        std::string *newSets = new std::string[amountOfSets + 1];
        for (int i = 0; i < amountOfSets; i++) {
            newSets[i] = sets[i];
        }
        newSets[amountOfSets] = set;
        sets = newSets;
        amountOfSets++;

        // We need to throw away the part of the string we already used.
        line = line.substr(line.find(Constants::SET_SEPARATOR) + 2, line.length() - line.find(Constants::SET_SEPARATOR) - 2);
    }

    // We need to get the last Set.
    std::string set = line;

    // We need to add the Set to the array of Sets.
    std::string *newSets = new std::string[amountOfSets + 1];
    for (int i = 0; i < amountOfSets; i++) {
        newSets[i] = sets[i];
    }
    newSets[amountOfSets] = set;
    sets = newSets;
    amountOfSets++;

    // Now we need to break each Set string into another substring for each color.
    // We can do this by splitting the Set string by the comma.
    std::string **colors = new std::string*[amountOfSets];
    for (int i = 0; i < amountOfSets; i++) {
        unsigned int amountOfColors = static_cast<unsigned int>(Types::Color::amountOfColors);
        std::string *setColors = new std::string[amountOfColors];
        unsigned short int counter = 0;
        while (sets[i].find(Constants::COLOR_SEPARATOR) != std::string::npos) {
            // We need to get the next color.
            std::string color = sets[i].substr(0, sets[i].find(Constants::COLOR_SEPARATOR));

            // We need to add the color to the array of colors.
            setColors[counter] = color;
            counter++;

            if (counter > amountOfColors) {
                // We need to throw an error here.
                throw new std::range_error("Invalid amount of colors in set");
            }

            // We need to throw away the part of the string we already used.
            sets[i] = sets[i].substr(sets[i].find(Constants::COLOR_SEPARATOR) + 2, sets[i].length() - sets[i].find(Constants::COLOR_SEPARATOR) - 2);
        }

        // We need to get the last color.
        std::string color = sets[i];

        // We need to add the color to the array of colors.
        setColors[counter] = color;
        counter++;

        if (counter > amountOfColors) {
            // We need to throw an error here.
            throw new std::range_error("Invalid amount of colors in set");
        }

        // We need to add the array of colors to the array of arrays of colors.
        colors[i] = setColors;
    }

    // Now we need to create the Sets.
    Set **setsArray = new Set*[amountOfSets];
    for (int i = 0; i < amountOfSets; i++) {
        // We need to get the amount of cubes of each color.
        unsigned int *amounts = new unsigned int[static_cast<unsigned int>(Types::Color::amountOfColors)];
        // Set every color to 0
        for (int j = 0; j < static_cast<unsigned int>(Types::Color::amountOfColors); j++) {
            amounts[j] = 0;
        }

        for (int j = 0; j < static_cast<unsigned int>(Types::Color::amountOfColors); j++) {
            unsigned int amount = ColorStringService::getColorAmount(colors[i][j]);
            Types::Color color = ColorStringService::getColorType(colors[i][j]);

            if (color == Types::Color::amountOfColors) {
                continue;
            }

            amounts[static_cast<unsigned int>(color)] = amount;
        }

        // We need to create the Set.
        Set *set = new Set(amounts[static_cast<unsigned int>(Types::Color::red)],
                           amounts[static_cast<unsigned int>(Types::Color::green)],
                           amounts[static_cast<unsigned int>(Types::Color::blue)]);

        // We need to add the Set to the array of Sets.
        setsArray[i] = set;

        // We need to delete the array of amounts.
        delete[] amounts;
    }

    // Now we need to create the Game.
    Game *game = new Game(id);

    // We need to add the Sets to the Game.
    for (int i = 0; i < amountOfSets; i++) {
        game->addSet(setsArray[i]);
    }

    // We need to delete the used pointers.
    delete[] setsArray;
    for (int i = 0; i < amountOfSets; i++) {
        delete[] colors[i];
    }
    delete[] colors;
    delete[] sets;
    
    return game;
}
