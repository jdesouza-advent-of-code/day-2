#ifndef LINE_PROCESSING_SERVICE_H
#define LINE_PROCESSING_SERVICE_H

#include <string>
#include "Types.h"
#include "Game.h"

class LineProcessingService {
    public:
        static Game *processLine(std::string line);
};

#endif // LINE_PROCESSING_SERVICE_H