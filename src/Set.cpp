#include "Set.h"
#include "Types.h"

/**
 * @brief Constructor of the Set class.
 * @param amountOfRed Amount of red cubes in the set.
 * @param amountOfGreen Amount of green cubes in the set.
 * @param amountOfBlue Amount of blue cubes in the set.
 */
Set::Set(int amountOfRed, int amountOfGreen, int amountOfBlue) {
    this->amountOfCubes = new int[static_cast<int>(Types::Color::amountOfColors)];
    this->amountOfCubes[static_cast<int>(Types::Color::red)] = amountOfRed;
    this->amountOfCubes[static_cast<int>(Types::Color::green)] = amountOfGreen;
    this->amountOfCubes[static_cast<int>(Types::Color::blue)] = amountOfBlue;
}

/**
 * @brief Destructor of the Set class.
 */
Set::~Set() {
    delete[] this->amountOfCubes;
}

/**
 * @brief Returns the amount of cubes of the given color.
 * @param color Color of the cubes.
 * @return Amount of cubes of the given color.
 */
int Set::amountOfCubesOfColor(Types::Color color) {
    return this->amountOfCubes[static_cast<int>(color)];
}
