#ifndef SET_H
#define SET_H

#include "Types.h"

class Set {
    private:
        int *amountOfCubes;
    public:
        Set(int amountOfRed, int amountOfGreen, int amountOfBlue);
        ~Set();
        int amountOfCubesOfColor(Types::Color color);
};

#endif // SET_H