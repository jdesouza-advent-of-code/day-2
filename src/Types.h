#ifndef COLOR_H
#define COLOR_H

namespace Types {
    enum class Color {
        red,
        blue,
        green,
        amountOfColors
    };

    enum class Errors {
        ok,
        fileNotFound,
        amountOfErrors
    };

    typedef unsigned int GameId;
}

#endif // COLOR_H
