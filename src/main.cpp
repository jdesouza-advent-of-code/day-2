#include <iostream>
#include "Types.h"
#include "InputReader.h"
#include "Game.h"
#include "Bag.h"
#include "Set.h"
#include "GameEvaluatorService.h"

#define OK                          0
#define INVALID_AMOUNT_OF_ARGS      1
#define INVALID_ARGUMENT            2

#define AMOUNT_OF_ARGS              3

#define AMOUNT_OF_RED_IN_A_BAG      12
#define AMOUNT_OF_GREEN_IN_A_BAG    13
#define AMOUNT_OF_BLUE_IN_A_BAG     14

// Arguments: path to file, filename
int main(int argc, char **argv) {
    Game *nextGame = NULL;

    if (argc != AMOUNT_OF_ARGS) {
        std::cout << "Invalid amount of arguments" << std::endl;
        exit(INVALID_AMOUNT_OF_ARGS);
    }

    char *path = argv[1];
    char *filename = argv[2];

    if (path == NULL || filename == NULL) {
        std::cout << "Invalid argument" << std::endl;
        exit(INVALID_ARGUMENT);
    }

    Bag *bag = new Bag(
        AMOUNT_OF_RED_IN_A_BAG,
        AMOUNT_OF_GREEN_IN_A_BAG,
        AMOUNT_OF_BLUE_IN_A_BAG
    );
    InputReader *inputReader = new InputReader(filename, path);

    std::cout << "Bag contains: R: " << bag->amountOfCubesOfColor(Types::Color::red) << ", G: " << bag->amountOfCubesOfColor(Types::Color::green) << ", B: " << bag->amountOfCubesOfColor(Types::Color::blue) << std::endl;

    Types::Errors error = inputReader->read();
    if (error != Types::Errors::ok) {
        std::cout << "Error reading file" << std::endl;
        exit(static_cast<int>(error));
    }

    int amountOfGames = inputReader->getAmountOfGames();

    std::cout << "Amount of games: " << amountOfGames << std::endl;

    nextGame = inputReader->popGame();
    unsigned int sumOfValidGameIds = 0;
    unsigned int powerOfTheGames = 0;
    while (nextGame != NULL) {
        int maximumAmountOfRedCubesInASet = nextGame->getMaximumAmountOfCubesOfColorInASet(Types::Color::red);
        int maximumAmountOfGreenCubesInASet = nextGame->getMaximumAmountOfCubesOfColorInASet(Types::Color::green);
        int maximumAmountOfBlueCubesInASet = nextGame->getMaximumAmountOfCubesOfColorInASet(Types::Color::blue);

        bool isValid = GameEvaluatorService::isGameValid(nextGame, bag);
        if (isValid) {
            sumOfValidGameIds += nextGame->getId();
        }
        powerOfTheGames += maximumAmountOfRedCubesInASet * maximumAmountOfGreenCubesInASet * maximumAmountOfBlueCubesInASet;

        delete nextGame;
        nextGame = inputReader->popGame();
    }

    std::cout << "Sum of valid game ids: " << sumOfValidGameIds << std::endl;
    std::cout << "Power of the games: " << powerOfTheGames << std::endl;

    return 0;
}